<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL & ~ NOTICE);

header('Content-Type: application/json; charset=utf8');
header("Expires: Thu, 19 Feb 1998 13:24:18 GMT");     					
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");    						
header("Cache-Control: no-cache, must-revalidate");     						
header("Cache-Control: post-check=0,pre-check=0");     					
header("Cache-Control: max-age=0");     						
header("Pragma: no-cache");
header("Connection: close"); 

//print_r($_SERVER['REMOTE_ADDR']); exit;

if($_SERVER['REMOTE_ADDR'] !== '10.24.2.111' 
&& $_SERVER['REMOTE_ADDR'] !== '130.193.108.80'
&& $_SERVER['REMOTE_ADDR'] !== '62.82.228.34'
&& $_SERVER['REMOTE_ADDR'] !== '176.37.96.18'        
&& $_SERVER['REMOTE_ADDR'] !== '10.24.2.164'
&& $_SERVER['REMOTE_ADDR'] !== '130.193.108.81'
&& $_SERVER['REMOTE_ADDR'] !== '130.193.108.82'
&& $_SERVER['REMOTE_ADDR'] !== '130.193.108.83'
&& $_SERVER['REMOTE_ADDR'] !== '130.193.108.84' 
&& $_SERVER['REMOTE_ADDR'] !== '130.193.108.85') 
	{ 
		echo 'access denied'; 
		exit;
	}       

$json = file_get_contents('php://input'); 
$clients = json_decode($json,1);

//print_r($clients); 

if(strlen($clients['name']) == 0){
    
    $err = array('status' => 0, "reason" => 'validation_errors', 'errors' => array('name' => 'value must not be empty!'));
    
    echo json_encode($err);
    
    exit();
}

if(strlen($clients['tell']) != 10){
    
    $err = array('status' => 0, "reason" => 'validation_errors', 'errors' => array('tell' => 'must be 10 digits!'));
    
    echo json_encode($err);
    
    exit();
}

if(strlen($clients['date_client']) != 0){ 
  
    if(!preg_match('/[\d]{4}-[\d]{2}-[\d]{2}/', $clients['date_client'])){
    
        $err = array('status' => 0, "reason" => 'validation_errors', 'errors' => array('date_client' => 'invalid data format!'));
        
        echo json_encode($err);
        
        exit();
    }
}

$name = mb_convert_encoding($clients['name'], "windows-1251", "utf-8");
$city = mb_convert_encoding($clients['city'], "windows-1251", "utf-8");
$address = mb_convert_encoding($clients['address'], "windows-1251", "utf-8");

mssql_connect('MSSQL_10.24.2.20', 'sa', 'alcatel') or exit();
mssql_select_db('OUTBOUNDMF');
mssql_query('SET ANSI_NULLS ON; SET ANSI_WARNINGS ON;');


$sql = " 
    INSERT INTO [dbo].[si_IMD_json]
           ([name]
           ,[tell]
           ,[email]
           ,[date_client]
           ,[city]
           ,[address]
           ,[set_date]
           ,[customId])
     VALUES
            (
            '{$name}'
           ,'{$clients['tell']}'
           ,'{$clients['email']}'
           ,'{$clients['date_client']}'
           ,'{$city}'
           ,'{$address}'
           ,GETDATE()
           ,'{$clients['customId']}'
            )";
           
//echo '<pre>'.$sql.'</pre>';           

if(mssql_query($sql)){
    
    $err = array('status' => 1, "request_id" => 112333, "claim_id" => $clients['customId']);
        
    echo json_encode($err);
        
} else {
    
    $err = array('status' => 0, "reason" => 'validation_errors', 'errors' => 'Something went wrong!');
        
    echo json_encode($err);
}


